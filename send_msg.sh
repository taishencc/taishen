#!/bin/bash

if [ $# != 3 ]; then
    echo "Usage $0 <msg_data> <at_me_on/off> <at_all_on/off>"
    exit -1
fi

function send_msg() {
    if [[ $2 = "on" && $3 = "off" ]]
    then
        msg_data='{"msgtype":"text","text":{"content":"'$1'"},"at":{"atMobiles":["18749678907"],"isAtAll":false}}'
    elif [[ $2 = "off" && $3 = "on" ]]
    then
        msg_data='{"msgtype":"text","text":{"content":"'$1'"},"at":{"atMobiles":["18749678907"],"isAtAll":true}}'
    elif [[ $2 = "off" && $3 = "off" ]]
    then
        msg_data='{"msgtype":"text","text":{"content":"'$1'"}}'
    else
        msg_data='{"msgtype":"text","text":{"content":"'$1'"}}'
    fi
    curl 'https://oapi.dingtalk.com/robot/send?access_token=c0c5e5e2052858e12c646d0e74ce59de9590a773a73bf7fd0d2bd9c871ac3d50' \
    -H 'Content-Type: application/json' \
    -d ''$msg_data''
}

send_msg $1 $2 $3
